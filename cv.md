---
title: CV
nocite: '@*'
---

**Olli Kilkki**  
`olli.kilkki at gmail.com`

--------------------------

## Technical background

My work experience includes *software and algorithm development, optimisation, modelling* in fields such as logistics and energy systems.

- Combinatorial problems
  - fleet management / scheduling in automated terminals
  - auction models for energy flexibility markets
- Problems involving uncertainties
  - scheduling of energy storages such as batteries and thermal loads
  - multi-level stochastic programming and robust optimisation

Also I have done *project management* and *design of system architectures* as well as their *development* & *integration*.
[See work history below for details ↴](#work-history)

Technologies actively in use include:

- `F#` / `.NET` [see later for Gitlab projects ↴](#personal-technical-projects)
  - algorithms, optimisation, linear programming ([Flips](https://flipslibrary.com/), [Google OR-Tools](https://developers.google.com/optimization/))
  - data analysis & visualisation ([FSharp.Stats](https://fslab.org/FSharp.Stats), [Deedle](https://fslab.org/Deedle/), [Plotly.NET](https://plotly.github.io/Plotly.NET))
- `Python`
  - optimisation & modelling (using e.g. [PuLP](https://coin-or.github.io/pulp/), [Pyomo](http://www.pyomo.org/))
  - event-driven programming (MQTT, asyncio, [trio](https://trio.readthedocs.io/en/stable/))
  - data analysis & visualisation ([Jupyter](https://jupyter.org/), [Streamlit](https://www.streamlit.io/) & [Altair](https://altair-viz.github.io/), [Dash](https://plotly.com/dash/) & [Plotly](https://plotly.com/)), [see below for an example ↴](#personal-technical-projects)
- General SWE technologies such as
  - integration, using e.g. REST, MQTT, GraphQL and JSON
  - Linux, SQL, containers & orchestration, Git

Previous experience:

- Mostly familiar, but it's been a while: `Matlab`, `Python` web development (django)
- A bit less familiar also with: `R`, `C`, `C++`, `Java`, `IEC-61131-3`, `Javascript`, `Simulink`, `Labview`

### Personal technical projects

- Random projects on [GitLab](https://gitlab.com/users/okilkki/projects):
  - Combinatorial auctions: [source](https://gitlab.com/okilkki/fscombibid), [docs](https://okilkki.gitlab.io/fscombibid/) (`F#`)
  - [Route finding website / service](https://gitlab.com/okilkki/reitti) (Solidabis code challenge, in `Python`)
  - [Data analysis of energy use and costs](https://gitlab.com/okilkki/energy-use-tracking) (Jupyter notebook in `Python`)
  - [CV](https://gitlab.com/okilkki/cv) automated generation from Markdown to [GitLab Pages](https://okilkki.gitlab.io/cv/) and as [PDF](https://okilkki.gitlab.io/cv/cv.pdf)
- [Team website](http://teekin.dy.fi) (*2011*): [Django](https://www.djangoproject.com/), nginx and MariaDB containers orchestrated locally on linux (Raspberry Pi), *currently offline*

## Work history

2021-now - **Kalmar / Cargotec**

- Senior software developer, fleet management
  - algorithms, optimisation, `F#` / `.NET`

2017-2021 - **Enerim** (Empower IM)

- *Product manager* for future energy market products
- Project management, funding
- Integration, conceptualization, prototypes, data analysis: `Python`, `VB (.NET)`
- Research in various EU Horizon 2020, ITEA and national collaborative research projects, [see publications below ↴](#personal-technical-projects)
  - energy and flexibility markets ([combinatorial auctions](http://dominoesproject.eu/wp-content/uploads/2020/10/D3.5_DOMINOES_Aggregation_based_DR_v1.0.pdf), [market platform architecture design](http://dominoesproject.eu/wp-content/uploads/2020/01/D2.2_DOMINOES_Scalable-Local-Energy-Market-Architecture_v1.1_PU_disclaimeradded.pdf))
  - demand response services

2013-2017 - **Aalto University** School of Electrical Engineering, Information Technologies in Industrial Automation

- *Doctoral student*
  - demand response modeling and optimisation: `Matlab`, `Python`, `C++`
- *Course assistant*

2012 - **Aalto University** Control Engineering

- *Research & course assistant*
  - event-driven model predictive control: `Matlab`, `Simulink`

2011 - **Aalto University** Software Business and Engineering Institute

- *Research assistant*
  - mobile software prototypes: `Python`

## Education

**Doctor of Science**, 2018, *with distinction*, from Aalto University School of Electrical Engineering, Automation and Systems Technology

- [Optimizing Demand Response of Aggregated Residential Energy Storages](https://aaltodoc.aalto.fi/handle/123456789/34068)

**Master of Science**, 2013, *with distinction*, from Aalto University School of Electrical Engineering, Automation and Systems Technology

- [Optimized Event-based Transmission and Control](https://aaltodoc.aalto.fi/handle/123456789/10913)

**Bachelor of Science**, 2011, from Aalto University School of Electrical Engineering

## Other interests

- Football and futsal ⚽, biking 🚲 (in good weather)
- Photography 📷 [^flickr] [^ig], reading [^goodreads] 🕮 (fiction and non-fiction)

## Research

3 published first author articles in academic journals and 3 in international conferences (21 in total).

Research in the areas of *smart grid, energy and flexibility markets, demand response, optimisation of heating load and energy storage scheduling, stochastic programming, wireless networks, event-driven model predictive control* and *simulation*.

### Publications

[^goodreads]: [37764020-olli@goodreads](https://www.goodreads.com/user/show/37764020-olli)
[^ig]: [ollikilkki@ig](https://www.instagram.com/ollikilkki/)
[^flickr]: [okilkki@flickr](https://www.flickr.com/photos/188615105@N02)
