# CV

Creates cv based on Markdown document on GitLab [Pages](https://okilkki.gitlab.io/cv/) and as a [PDF](https://okilkki.gitlab.io/cv/cv.pdf).

## Pandoc commands

HTML

```console
pandoc -s cv.md -o public/index.html --lua-filter=current-date.lua -c style.css --csl ieee.csl --bibliography published.bib
```

PDF

```console
pandoc -s cv.md -o public/cv.pdf --lua-filter=current-date.lua --csl ieee.csl --bibliography published.bib --pdf-engine=xelatex
```

## TODO

- better styling / CSS
- more details?
